// #define BOUNCE_LOCK_OUT
#include <Bounce2.h>

#define USE_TIMER_1 true
#include <TimerInterrupt_Generic.h>

#define LED (10)
#define AUDIO_PIN (13)
#define F_AUDIO (440) // Hz
#define TRIGGER (2)
#define TRIG_DELAY_MIN (500) //Don't trigger faster than twice per second

bool audio = 0;
void TimerHandler() {
  audio = !audio;
  digitalWrite(AUDIO_PIN, !audio);
}

Bounce bounce = Bounce();

void setup() {
  /* Serial port */
  Serial.begin(9600);

  /* LED indicator */
  pinMode(LED, OUTPUT);
  digitalWrite(LED, 1);

  /* Trigger */
  bounce.attach(TRIGGER, INPUT_PULLUP); // USE INTERNAL PULL-UP
  bounce.interval(10); // interval in ms

  /* Audio */
  pinMode(AUDIO_PIN, OUTPUT);
  digitalWrite(AUDIO_PIN, 0);
  ITimer1.init();

  Serial.println("We beginnen");
}

void trigger() {
  ITimer1.attachInterrupt(F_AUDIO*2, TimerHandler, 50);
}

bool trig_state;
long last_trigger = 0;

void loop()
{
  bounce.update();
  if (bounce.changed()) {
    trig_state = bounce.read();
    Serial.print(millis());
    Serial.print(" - ");
    Serial.print(trig_state);
    if(trig_state == LOW) {
      if(millis() - last_trigger < TRIG_DELAY_MIN) {
        Serial.print(" - Ignoring - delay too small");
      } else {
        last_trigger = millis();
        trigger();
        Serial.print(" > ON!");
        digitalWrite(LED, 0);
      }
    } else {
    }
    Serial.println();
  } else {
    digitalWrite(LED, 1);

  }
}